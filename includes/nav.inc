                <!-- Nav -->
                    <nav id="nav">
                        <ul class="links">
                            <li<tmpl_if name="tab-active-index"> class="active"</tmpl_if>><a href="index.html">Presentation</a></li>
                            <li<tmpl_if name="tab-active-components"> class="active"</tmpl_if>><a href="components.html">Components</a></li>
                            <li<tmpl_if name="tab-active-contact"> class="active"</tmpl_if>><a href="contact.html">Contact</a></li>
                        </ul>
                        <ul class="icons">
                            <li><a href="https://floss.social/@fusioniam" target="_blank" class="icon brands fa-mastodon"><span class="label">Mastodon</span></a></li>
                            <li><a href="https://www.facebook.com/FusionIAM" target="_blank" class="icon brands fa-facebook-f"><span class="label">Facebook</span></a></li>
                            <li><a href="https://gitlab.ow2.org/fusioniam/fusioniam" target="_blank" class="icon brands fa-gitlab"><span class="label">OW2 Gitlab</span></a></li>
                        </ul>
                    </nav>
